{pkgs ? import <nixpkgs> {}}:
pkgs.mkShell {
  # nativeBuildInputs is usually what you want -- tools you need to run
  packages = with pkgs; [
    python311
    python311Packages.dbus-python
    openssl
    simpleTpmPk11
    which
    cacert
    bash
    firefox
    xdg-utils
    hostname
    networkmanager
  ];

  shellHook = ''
    patch ./SecureW2_JoinNow.run ./JoinNow.patch -b
    bash ./SecureW2_JoinNow.run
    cd tmp/*
    patch detect.py ../../python.patch
    python3 main.py
    cd ../../
    rm -r tmp
    mv ./SecureW2_JoinNow.run.orig ./SecureW2_JoinNow.run
    #exit
  '';

  meta = with pkgs.lib; {
    homepage = "";
    description = "Generic Eduroam join script";
    longDescription = ''
      A generic Eduroam join script based on the process
      for NCSU's Eduroam instructions.
    '';
    license = licenses.gpl3Plus;
  };
}
