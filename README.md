# Eduroam Script

 This is a script to get eduroam working out of the box on various Linux devices. While it was initially made for personal use with [nixOS](https://nixos.org/), it can be used on any Linux system.

## Dependencies
The script makes use of the [nix package manager](https://nixos.org/download.html) to provide dependencies. Follow the installation instructions on the official website. Additionally, the target system must use Network-Manager (networkmanager).

If you can't install `nix` on your host machine, you can give [nix-portable](https://github.com/DavHau/nix-portable) a try but note that it may not work.

## Instructions
1. Clone and enter the repo. This assumes you have created a `~/git` directory, but you may clone it elsewhere if you prefer. If you haven't already, you can create the directory using `mkdir ~/git`.
```console
$ cd ~/git
$ git clone https://gitlab.com/ncsulug/eduroam-script.git
$ cd eduroam-script
```
2. Obtain `SecureW2_JoinNow.run` from https://go.ncsu.edu/eduroam2 and move it into the `eduroam-script` directory. If it is in `~/Downloads`, you may move it as follows:
```console
$ mv ~/Downloads/SecureW2_JoinNow.run .
```
3. Run `nix-shell` (or `/path/to/nix-portable nix-shell`) and follow the instructions.
